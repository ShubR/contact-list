import ContactCard from './ContactCard';

const ContactList = ({ contacts, onDelete }) => {


  return (
    <div className="ui celled list">     

      {contacts.length > 0 &&
        contacts.map((contact) => <ContactCard contact={contact} key={contact.id} onDelete={onDelete} />)}
    </div>
  );
};

export default ContactList;
