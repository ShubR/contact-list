import React from 'react';
import user from '../images/user.png';

const ContactCard = ({ contact, onDelete }) => {
  const { name, email, id } = contact;

  return (
    <div className="item">
      <img src={user} className="ui avatar image" alt="user" />
      <div className="content">
        <div className="header">{name}</div>
        <div>{email}</div>
      </div>
      <i
        className="trash alternate outline icon"
        style={{
          color: 'red',
          marginTop: '7px',
          float: 'right',
          cursor: 'pointer',
        }}
        onClick={() => onDelete(id)}
      ></i>
    </div>
  );
};

export default ContactCard;
