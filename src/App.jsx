import { useState } from 'react';
import './App.css';
import Header from './components/Header';
import AddContact from './components/AddContact';
import ContactList from './components/ContactList';
import { useEffect } from 'react';

function App() {
  const [contacts, setContacts] = useState([]);

  const onAddContact = (contact) => {
    setContacts([...contacts, contact]);
  };

  const onDelete = (id) => {
    const updatedContacts = contacts.filter((contact) => contact.id !== id);
    setContacts(updatedContacts);
  };

  useEffect(() => {
    const contactListString = localStorage.getItem('contacts');

    let contactList = [];

    if (contactListString) {
      contactList = JSON.parse(localStorage.getItem('contacts'));
      contactList.length > 0 && setContacts(contactList);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('contacts', JSON.stringify(contacts));
  }, [contacts]);

  return (
    <div className="ui container">
      <Header />
      <AddContact onAddContact={onAddContact} />
      <ContactList contacts={contacts} onDelete={onDelete} />
    </div>
  );
}

export default App;
